/*
 * IIgs .console driver input
 *
 */

#ifdef __ORCAC__
segment "gsos";
#pragma debug 0x8000
#endif

#include "console.h"
#include "gsos_frotz.h"

#include <gsos.h>
#include <misctool.h>

#include <stdlib.h>
#include <string.h>

extern int completion (const zchar *, zchar *);


static unsigned char OldTextPort[2 + 24 * 80];
int OldTextPortSize;

static InputPortRec OldInputPort;


static void abort_input(void) {

	deviceDCB.code = AbortInput;
	deviceDCB.list = (Pointer)""; // can't be null...
	deviceDCB.requestCount = 0;
	DControl(&deviceDCB);
}

void iigs_start_box(void) {

	deviceDCB.code = SaveTextPort;
	deviceDCB.list = OldTextPort;
	deviceDCB.requestCount = sizeof(OldTextPort);
	DStatusGS(&deviceDCB);
	OldTextPortSize = deviceDCB.transferCount;


	deviceDCB.code = GetInputPort;
	deviceDCB.list = (Pointer)&OldInputPort;
	deviceDCB.requestCount = sizeof(OldInputPort);
	DStatusGS(&deviceDCB);

	abort_input();

	// push the text port and set up a new port for our window.

	static char init80[] = {
    "\x01\x02\x2a\x28\x66\x30\x19\x1b\x15\x13"
    "\xda\xcc\xcc\xcc\xcc\xcc\xcc\xcc\xcc\xcc"
    "\xcc\xcc\xcc\xcc\xcc\xcc\xcc\xcc\xcc\xcc"
    "\xcc\xcc\xcc\xcc\xcc\xcc\xcc\xcc\xcc\xcc"
    "\xcc\xcc\xcc\xcc\xcc\xcc\xcc\xcc\xcc\xcc"
    "\xcc\xcc\xcc\xcc\xcc\xcc\xcc\xcc\xcc\xcc"
    "\xcc\xcc\xcc\xcc\xcc\xcc\xcc\xcc\xcc\xdf"
    "\x0d\xda\x10\x5a\xdf\x0d\xda\x10\x5a\xdf"
    "\x0d\xda\x10\x5a\xdf\x0d\xda\x10\x5a\xdf"
    "\x0d\xda\x10\x5a\xdf\x0d\xda\x10\x5a\xdf"
    "\x0d\xda\x5f\x5f\x5f\x5f\x5f\x5f\x5f\x5f"
    "\x5f\x5f\x5f\x5f\x5f\x5f\x5f\x5f\x5f\x5f"
    "\x5f\x5f\x5f\x5f\x5f\x5f\x5f\x5f\x5f\x5f"
    "\x5f\x5f\x5f\x5f\x5f\x5f\x5f\x5f\x5f\x5f"
    "\x5f\x5f\x5f\x5f\x5f\x5f\x5f\x5f\x5f\x5f"
    "\x5f\x5f\x5f\x5f\x5f\x5f\x5f\x5f\x5f\x5f"
    "\xdf\x02\x2c\x29\x63\x2d\x19"
	};
	static char init40[] = {
    "\x01\x02\x22\x28\x46\x30\x19\x1b\x15\x13"
    "\xda\xcc\xcc\xcc\xcc\xcc\xcc\xcc\xcc\xcc"
    "\xcc\xcc\xcc\xcc\xcc\xcc\xcc\xcc\xcc\xcc"
    "\xcc\xcc\xcc\xcc\xcc\xcc\xcc\xcc\xcc\xcc"
    "\xcc\xcc\xcc\xcc\xcc\xdf\x0d\xda\x10\x42"
    "\xdf\x0d\xda\x10\x42\xdf\x0d\xda\x10\x42"
    "\xdf\x0d\xda\x10\x42\xdf\x0d\xda\x10\x42"
    "\xdf\x0d\xda\x10\x42\xdf\x0d\xda\x5f\x5f"
    "\x5f\x5f\x5f\x5f\x5f\x5f\x5f\x5f\x5f\x5f"
    "\x5f\x5f\x5f\x5f\x5f\x5f\x5f\x5f\x5f\x5f"
    "\x5f\x5f\x5f\x5f\x5f\x5f\x5f\x5f\x5f\x5f"
    "\x5f\x5f\xdf\x02\x24\x29\x43\x2d\x19"
	};

	if (gs_setup.screen_width == 40) {
		ioDCB.requestCount = sizeof(init40) - 1;
		ioDCB.buffer = init40;
	} else {
		ioDCB.requestCount = sizeof(init80) - 1;
		ioDCB.buffer = init80;
	}
	DWrite(&ioDCB);
}

void iigs_end_box(void) {

	// reset...
	static char restore[] = {
		scPopTextPort
	};
	ioDCB.requestCount = sizeof(restore);
	ioDCB.buffer = restore;
	DWrite(&ioDCB);

	// restore the screen...
	deviceDCB.code = RestoreTextPort;
	deviceDCB.list = OldTextPort;
	deviceDCB.requestCount = OldTextPortSize;
	DControlGS(&deviceDCB);


	// and input port.
	deviceDCB.code = SetInputPort;
	deviceDCB.list = (Pointer)&OldInputPort;
	deviceDCB.requestCount = sizeof(OldInputPort);
	DControlGS(&deviceDCB);

}



#if 0
static int prompt_quit(void) {

	#define TEXT "13/Quit Game?/^Yes/No\x00" "100"
	static struct {
		unsigned offset1;
		unsigned offset2;
		char text[sizeof(TEXT)];
	} msg = { 4, 2, "13/Quit Game?/^Yes/No\x00" "100" }; // TEXT doesn't generate correctly.
	// 0 would be no icon but there's a gs/os bug where 0 is treated as a custom icon.
	#undef TEXT

	int ok;
	ok = AlertMessage((Ptr)&msg, 0, NULL);
	return ok == 1;
}
#endif

static int prompt(const char *text) {

	uint16_t mode;
	iigs_start_box();

	ioDCB.buffer = text;
	ioDCB.requestCount = strlen(text);
	DWrite(&ioDCB);

	// raw mode.

	deviceDCB.code = SetReadMode;
	deviceDCB.list = (Pointer)&mode;
	deviceDCB.requestCount = 2;
	deviceDCB.transferCount = 0; 

	mode = 0x8000; // raw
	DControl(&deviceDCB);




	static char yesno[] = {
		scDisableMouse,
		// 1
		scGotoXY, 32 + 00, 32 + 4, scSetNormal, ' ', 'N', 'o', ' ', scSetNormal,
		// 10
		' ', ' ',
		// 12
		scSetNormal,
		' ', 'Y', 'e', 's', ' ',
		scSetNormal
	};
	// total text width = 11

	if (gs_setup.screen_width == 40) {
		yesno[2] = 32 + 36 - 6 - 11;
	} else {
		yesno[2] = 32 + 60 - 6 - 11;
	}

	int st = 1;
	char c;
	for(;;) {


		if (st == 1) {
			yesno[12] = scSetInverse;
			yesno[4] = scSetNormal;
		} else {
			yesno[12] = scSetNormal;
			yesno[4] = scSetInverse;
		}

		ioDCB.buffer = yesno;
		ioDCB.requestCount = sizeof(yesno);
		DWrite(&ioDCB);

		c = 0;
		ioDCB.buffer = &c;
		ioDCB.requestCount = 1;
		DRead(&ioDCB);

		if (c == 0x0d) break;
		if (c == 0x1b) {
			st = 0;
			break;
		}
		if (c == 0x08) {
			// <-
			if (st == 1) st = 0;
			continue;
		}
		if (c == 0x09) {
			// tab
			st = !st;
			continue;
		}
		if (c == 0x15) {
			// ->
			if (st == 0) st = 1;
			continue;
		}
	}

	// 
	mode = 0x0000;
	DControl(&deviceDCB);

	iigs_end_box();
	return st;
}

#if 0
#define __text__ "13/*0/^Yes/No\x00" "100"
static int prompt(const char *text) {


	static struct {
		unsigned offset1;
		unsigned offset2;
		char text[sizeof(__text__)];
	} template = { 4, 2, __text__ };
	// 0 would be no icon but there's a gs/os bug where 0 is treated as a custom icon.

	return AlertMessage((Ptr)&template, 0, (Ptr)&text);
}
	#undef __text__
#endif

zchar os_read_key (int timeout, bool show_cursor)
{
	uint8_t c;
	uint16_t mode = 0;

	uint32_t tick;

	ioDCB.buffer = &c;
	ioDCB.requestCount = 1;
	ioDCB.transferCount = 0;

	deviceDCB.code = SetReadMode;
	deviceDCB.list = (Pointer)&mode;
	deviceDCB.requestCount = 2;
	deviceDCB.transferCount = 0; 

	/* put the console driver in raw mode */
	mode = 0x8000;
	DControl(&deviceDCB);

	deviceDCB.code = setWaitStatus;

	if (timeout) {
		/* no-wait mode */
		mode = 0x8000;
		DControl(&deviceDCB);
		// timout is in 10ths of a second, ticks is 60/sec
		tick = GetTick() + timeout * 6;
		for(;;) {
			DRead(&ioDCB);
			if (ioDCB.transferCount) break;
			if (GetTick() > tick) {
				abort_input();
				return ZC_TIME_OUT;
			}
		}
	} else {
		mode = 0x0000;
		DControl(&deviceDCB);

		for(;;) {
			DRead(&ioDCB);
			if (ioDCB.transferCount) break;
		}
	}

	// todo -- getinput port to check terminator character for modifiers?
	if (c == 0x7f) return ZC_BACKSPACE;
	if (c >= 0x20) return c;
	switch(c) {
	case 0x08: return ZC_ARROW_LEFT;
	case 0x0a: return ZC_ARROW_DOWN;
	case 0x0b: return ZC_ARROW_UP;
	case 0x15: return ZC_ARROW_RIGHT;

	default:
	case 0x0d:
		return ZC_RETURN;
	}

} /* os_read_key */


static GSString255 *history[16] = { 0 };
static unsigned history_index = 0;


static void erase_input(InputPortRec *ip) {
	static char command[] = {
		0x1e, -1, -1, // goto
		0x1d // clear to end of line
	};
	command[1] = ip->originH + ' ';
	command[2] = ip->originV + ' ';
	ioDCB.buffer = command;
	ioDCB.requestCount = sizeof(command);
	DWrite(&ioDCB);
}

// TODO -- buf could have data in it (from a hotkey interrupt)
zchar os_read_line (int max, zchar *buf, int timeout, int UNUSED(width), int continued)
{
	static InputPortRec ip;

	uint16_t mode = 0;
	uint32_t tick;

	ioDCB.buffer = buf;
	ioDCB.requestCount = max;
	ioDCB.transferCount = 0;

	deviceDCB.code = SetReadMode;
	deviceDCB.list = (Pointer)&mode;
	deviceDCB.requestCount = 2;
	deviceDCB.transferCount = 0; 

	/* put the console driver in user-input mode */
	mode = 0x0000;
	DControl(&deviceDCB);

	deviceDCB.code = setWaitStatus;

	// todo -- continued support...

	// todo -- timeout read should support interrupt chars as well...
	if (timeout) {
		/* no-wait mode */
		mode = 0x8000;
		DControl(&deviceDCB);
		// timout is in 10ths of a second, ticks is 60/sec
		tick = GetTick() + timeout * 6;
		for(;;) {
			DRead(&ioDCB);
			if (ioDCB.transferCount) break;
			if (GetTick() > tick) {
				abort_input();
				return ZC_TIME_OUT;
			}
		}
	} else {
		/* wait mode */
		mode = 0x0000;
		DControl(&deviceDCB);
		unsigned index = history_index;

		for(;;) {

retry:
			DRead(&ioDCB);

			deviceDCB.code = GetInputPort;
			deviceDCB.list = (Pointer)&ip;
			deviceDCB.requestCount = sizeof(ip);

			DStatus(&deviceDCB);

			// todo -- hotkeys - ZC_HKEY_UNDO, ZC_HKEY_RESTART

			if (ip.entryType != 1) break;
				// interrupt entry / exit
			switch (ip.lastTermChar) {

				case 0x0a: // down arrow
					index = (index + 1) & 0x0f;
					break;
				case 0x0b: // up arrow
					index = (index - 1) & 0x0f;
					break;

				case 0x09: { // tab completion?

					int status = 2;
					if (ip.cursorPos == ip.inputLength) {
						static zchar extension[10];

						buf[ip.inputLength] = 0;
						status = completion(buf, extension);
						int n = strlen(extension);

						if (n) {
							if (ip.inputLength + n < max) {
								memcpy(buf + ip.inputLength, extension, n);
								ip.inputLength += n;
								ip.cursorPos += n;
								deviceDCB.code = SetInputPort;
								DControl(&deviceDCB);
							}
						}
					}
					if (status != 0) SysBeep();
					goto retry;
					break;
				}

				case 'q':
				case 'Q':

					abort_input();
					erase_input(&ip);
					if (prompt("\pQuit Game?")) {
						buf[0] = 0;
						z_quit();
						return 0xff;
					}

					// these get clobbered...
					ioDCB.buffer = buf;
					ioDCB.requestCount = max;
					ioDCB.transferCount = 0;

					goto retry;
					break;

				case 'r':
				case 'R':

					abort_input();
					erase_input(&ip);

					if (prompt("\pRestart Game?")) {
						buf[0] = 0;
						z_restart();
						return 0xff;
					}

					ioDCB.buffer = buf;
					ioDCB.requestCount = max;
					ioDCB.transferCount = 0;

					goto retry;
					break;


				case 'o':
				case 'O':
					/* open - do a restore command */
					if (max >= 8) {
						abort_input();
						erase_input(&ip);
						strcpy(buf, "restore");
						return ZC_RETURN;
					}
					SysBeep();
					goto retry;
					break;

				case 's':
				case 'S':
					/* save - do a save command */
					if (max >= 8) {
						abort_input();
						erase_input(&ip);
						strcpy(buf, "save");
						return ZC_RETURN;
					}
					SysBeep();
					goto retry;
					break;

				case 'z':
				case 'Z': {
					int restore_undo(void);

					// see hot_key_undo()

					// -1 - undo disabled
					// 0 - no undo slots
					// 2 - undone.
					int ok = restore_undo();
					if (ok < 1) {
						SysBeep();
						goto retry;
					}

					if (z_header.version >= V5) {
						/* for V5+ games we must */
						/* store 2 (for success) */
						/* and abort the input   */
						abort_input();
						erase_input(&ip);
						buf[0] = 0;

						store(2);
						return 0xff;
					}
					if (z_header.version <= V3) {
						/* for V3- games we must */
						/* draw the status line  */
						/* and continue input    */
						// extern int current_style;
						// int old_style = current_style;
						abort_input();
						erase_input(&ip);
						buf[0] = 0;

						z_show_status();
						flush_buffer(); // fixup reverse mode?

						return ZC_BAD;
					}

					goto retry;
					break;
				}
			}


			erase_input(&ip);

			ioDCB.buffer = buf;
			ioDCB.requestCount = max;
			ioDCB.transferCount = 0;

			// todo -- copy history from dos
			GSString255 *str = history[index];
			if (str) {
				unsigned len = str->length;
				if (len > max) len = max;
				ip.inputLength = len;
				ip.cursorPos = len;
				memcpy(buf, str->text, len);
				// deviceDCB.requestCount = str->length;
				// deviceDCB.list = str->text;
			} else {
				ip.inputLength = 0;
				ip.cursorPos = 0;
				// deviceDCB.requestCount = 0;
				// deviceDCB.list = "";
			}
			deviceDCB.code = SetInputPort;
			DControl(&deviceDCB);
			// deviceDCB.code = SetDefaultString;
			// DControl(&deviceDCB);
		}

		#if 0
		// clear the default string.
		deviceDCB.list = "";
		deviceDCB.requestCount = 0;
		deviceDCB.code = SetDefaultString;
		DControl(&deviceDCB);
		#endif

		// save it in the  history buffer...
		unsigned len = ioDCB.transferCount;
		if (len) {
			GSString255 *str = malloc(2 + len);
			str->length = len;
			memcpy(str->text, buf, len);

			history[history_index] = str;
			history_index = (history_index + 1) & 0x0f;
			if (history[history_index]) {
				free(history[history_index]);
				history[history_index] = NULL;
			}
		}
	}

	// 0-terminate buffer
	max = ioDCB.transferCount;
	buf[max] = 0;
	return ZC_RETURN;
} /* os_read_line */




/*
 * os_read_file_name
 *
 * Return the name of a file. Flag can be one of:
 *
 *    FILE_SAVE      - Save game file
 *    FILE_RESTORE   - Restore game file
 *    FILE_SCRIPT    - Transcript file
 *    FILE_RECORD    - Command file for recording
 *    FILE_PLAYBACK  - Command file for playback
 *    FILE_SAVE_AUX  - Save auxilary ("preferred settings") file
 *    FILE_LOAD_AUX  - Load auxilary ("preferred settings") file
 *    FILE_NO_PROMPT - Return file without prompting the user
 *
 * The length of the file name is limited by MAX_FILE_NAME. Ideally
 * an interpreter should open a file requester to ask for the file
 * name. If it is unable to do that then this function should call
 * print_string and read_string to ask for a file name.
 *
 * Return value is NULL if there was a problem.
 */
char *os_read_file_name (const char *default_name, int flag)
{
	if (gs_setup.desktop) {
		char *gsos_read_file_name(const char *default_name, int flag);
		return gsos_read_file_name(default_name, flag);
	}

	static char buffer[80];
	static InputPortRec ip;

	iigs_start_box();

	static char terms[] = {
		0x7f, 0x80, // term mask
		3,    0x00, // 3 terminators
		0x0d, 0x00, // return.
		0x1b, 0x00, // esc
		'.',  0x80, // command - .
	};


	deviceDCB.code = SetTerminators;
	deviceDCB.list = terms;
	deviceDCB.requestCount = sizeof(terms);
	DControlGS(&deviceDCB);



	switch(flag) {
		case FILE_RESTORE:
			ioDCB.buffer = "Open File:\r";
			ioDCB.requestCount = 11;
			DWrite(&ioDCB);
			break;
		case FILE_SAVE:
		case FILE_SCRIPT:
			ioDCB.buffer = "Save File:\r";
			ioDCB.requestCount = 11;
			DWrite(&ioDCB);
			break;
	}

	// write some descriptive text
	ioDCB.buffer = buffer;
	ioDCB.requestCount = sizeof(buffer) - 1;
	DRead(&ioDCB);

	deviceDCB.code = GetInputPort;
	deviceDCB.list = (Pointer)&ip;
	deviceDCB.requestCount = sizeof(ip);
	DStatus(&deviceDCB);

	iigs_end_box();

	gsos_setup_terms();

	// now check the input port details.
	// 1 = return, 2 = esc, 3 = command-.
	if (ip.exitType != 1) return NULL;
	unsigned l = ioDCB.transferCount;
	if (l == 0) return NULL;
	buffer[l] = 0;
	return buffer;
} /* os_read_file_name */

zword os_read_mouse(void)
{
	/* NOT IMPLEMENTED */
	return 0;
} /* os_read_mouse */

void os_tick(void)
{
	/* Nothing here yet */
} /* os_tick */

/* Generated on Wed Jan  3 18:21:16 2024 */
#include "types.rez"

resource rComment(0x00000001) {
	"\n"
	"FROTZ V2.55\n"
	"An interpreter for all Infocom a"
	"nd other Z-Machine games.\n"
	"Complies with the Z-Machine Stan"
	"dard version 1.1.\n"
	"\n"
	"Originally written by Stefan Jok"
	"isch in 1995-1997.\n"
	"Ported to Unix by Galen Hazelwoo"
	"d.\n"
	"Reference code Unix and DOS port"
	"s are maintained by David Griffi"
	"th.\n"
	"IIgs .console port by Kelvin She"
	"rlock.\n"
	"\n"
	"Frotz is free software; you can "
	"redistribute it and/or modify it"
	" under the terms of the GNU Gene"
	"ral Public License as published "
	"by the Free Software Foundation;"
	" either version 2 of the License"
	", or (at your option) any later "
	"version.\n"
};

resource rVersion(0x00000001) {
	{ $00, $00, $00, alpha, $02 }, /* version */
	verUS, /* region */
	"Frotz 2.55", /* short name */
	"Kelvin Sherlock\nDavid Griffith, et alia" /* more info */
};

resource rToolStartup(0x00000001) {
	0xc080, /* mode */
	{
		3, 0x0000,
		4, 0x0308,
		5, 0x0000,
		6, 0x0000,
		11, 0x0000,
		14, 0x0000,
		15, 0x0000,
		16, 0x0000,
		18, 0x0000,
		20, 0x0000,
		21, 0x0000,
		22, 0x0000,
		23, 0x0000,
		27, 0x0000,
		28, 0x0000,
		30, 0x0000
	}
};

resource rMenuItem(0x00000100) {
	0x0100, /* id */
	"", "", /* chars */
	0x0000, /* check */
	0x8000, /* flags */
	0x0001 /* title ref (rPString) */
};

resource rMenuItem(0x00000101) {
	0x0101, /* id */
	",", ",", /* chars */
	0x0000, /* check */
	0x8000, /* flags */
	0x0002 /* title ref (rPString) */
};

resource rMenuItem(0x00000102) {
	0x0102, /* id */
	"", "", /* chars */
	0x0000, /* check */
	0x8080, /* flags */
	0x0003 /* title ref (rPString) */
};

resource rMenuItem(0x00000103) {
	0x0103, /* id */
	"O", "o", /* chars */
	0x0000, /* check */
	0x8000, /* flags */
	0x0004 /* title ref (rPString) */
};

resource rMenuItem(0x00000104) {
	0x0104, /* id */
	"R", "r", /* chars */
	0x0000, /* check */
	0x8000, /* flags */
	0x0005 /* title ref (rPString) */
};

resource rMenuItem(0x00000105) {
	0x0105, /* id */
	"", "", /* chars */
	0x0000, /* check */
	0x8080, /* flags */
	0x0006 /* title ref (rPString) */
};

resource rMenuItem(0x00000106) {
	0x0106, /* id */
	"Q", "q", /* chars */
	0x0000, /* check */
	0x8000, /* flags */
	0x0007 /* title ref (rPString) */
};

resource rMenuItem(0x00000107) {
	0x00fa, /* id */
	"Z", "z", /* chars */
	0x0000, /* check */
	0x8000, /* flags */
	0x0008 /* title ref (rPString) */
};

resource rMenuItem(0x00000108) {
	0x0108, /* id */
	"", "", /* chars */
	0x0000, /* check */
	0x8080, /* flags */
	0x0009 /* title ref (rPString) */
};

resource rMenuItem(0x00000109) {
	0x00fb, /* id */
	"X", "x", /* chars */
	0x0000, /* check */
	0x8000, /* flags */
	0x000a /* title ref (rPString) */
};

resource rMenuItem(0x0000010a) {
	0x00fc, /* id */
	"C", "c", /* chars */
	0x0000, /* check */
	0x8000, /* flags */
	0x000b /* title ref (rPString) */
};

resource rMenuItem(0x0000010b) {
	0x00fd, /* id */
	"V", "v", /* chars */
	0x0000, /* check */
	0x8000, /* flags */
	0x000c /* title ref (rPString) */
};

resource rMenuItem(0x0000010c) {
	0x00fe, /* id */
	"", "", /* chars */
	0x0000, /* check */
	0x8000, /* flags */
	0x000d /* title ref (rPString) */
};

resource rPString(0x00000001) {
	"About Frotz\$c9"
};

resource rPString(0x00000002) {
	"Preferences\$c9"
};

resource rPString(0x00000003) {
	"-"
};

resource rPString(0x0000000e) {
	"@"
};

resource rPString(0x00000004) {
	"Open\$c9"
};

resource rPString(0x00000005) {
	"Restart"
};

resource rPString(0x00000006) {
	"-"
};

resource rPString(0x00000007) {
	"Quit"
};

resource rPString(0x0000000f) {
	" File "
};

resource rPString(0x00000008) {
	"Undo"
};

resource rPString(0x00000009) {
	"-"
};

resource rPString(0x0000000a) {
	"Cut"
};

resource rPString(0x0000000b) {
	"Copy"
};

resource rPString(0x0000000c) {
	"Paste"
};

resource rPString(0x0000000d) {
	"Clear"
};

resource rPString(0x00000010) {
	" Edit "
};

resource rMenu(0x00000001) {
	0x0001, /* menu ID */
	0xa000, /* flags */
	0x0000000e, /* title ref (rPString) */
	{
		0x00000100,
		0x00000101,
		0x00000102
	}
};

resource rMenu(0x00000002) {
	0x0002, /* menu ID */
	0xa000, /* flags */
	0x0000000f, /* title ref (rPString) */
	{
		0x00000103,
		0x00000104,
		0x00000105,
		0x00000106
	}
};

resource rMenu(0x00000003) {
	0x0003, /* menu ID */
	0xa000, /* flags */
	0x00000010, /* title ref (rPString) */
	{
		0x00000107,
		0x00000108,
		0x00000109,
		0x0000010a,
		0x0000010b,
		0x0000010c
	}
};

resource rMenuBar(0x00000001) {
	{
		0x00000001,
		0x00000002,
		0x00000003
	}
};

resource rTextForLETextBox2(0x00000001) {
	"\$01F\$05\$00\$00\$18\$01J\$01\$00Frotz"
};

resource rTextForLETextBox2(0x00000002) {
	"\$01J\$00\$00\$01F\$04\$00\$00\tFrotz v 2.55\n"
	"  by Stefan Jokisch, Galen Hazel"
	", David Griffith, et alia\n"
	"IIgs v 0.0.0 alpha 3\n"
	"  by Kelvin Sherlock"
};

resource rTextForLETextBox2(0x00000003) {
	"\$01J\$00\$00\$01F\$04\$00\$00\tFrotz is free software"
	"; you can redistribute it and/or"
	" modify it under the terms of th"
	"e GNU General Public License as "
	"published by the Free Software F"
	"oundation; either version 2 of t"
	"he License, or (at your option) "
	"any later version.\n"
	"\n"
	"This program contains material f"
	"rom the ORCA/C Run-Time Librarie"
	"s, copyright 1987-2024 by Byte W"
	"orks, Inc. Used with permission."
};

resource rControlTemplate(0x00000001) {
	0x00000001, /* control ID */
	{ 0, 0, 40, 400 }, /* rect */
	StatTextControl {{
		0xff00, /* flags */
		0x1002, /* more flags */
		0x00000000, /* refcon */
		0x00000001, /* text ref (rTextForLETextBox2) */
		0x0000, /* text size */
		0, /* text justification */
	}}
};

resource rControlTemplate(0x00000002) {
	0x00000002, /* control ID */
	{ 40, 10, 100, 390 }, /* rect */
	StatTextControl {{
		0xff00, /* flags */
		0x1002, /* more flags */
		0x00000000, /* refcon */
		0x00000002, /* text ref (rTextForLETextBox2) */
		0x0000, /* text size */
		0, /* text justification */
	}}
};

resource rControlTemplate(0x00000003) {
	0x00000003, /* control ID */
	{ 0, 10, 100, 390 }, /* rect */
	StatTextControl {{
		0xff80, /* flags */
		0x1002, /* more flags */
		0x00000000, /* refcon */
		0x00000003, /* text ref (rTextForLETextBox2) */
		0x0000, /* text size */
		0, /* text justification */
	}}
};

resource rWindParam1(0x00000001) {
	0x0020, /*frame bits */
	0x00000000, /* title */
	0x00000000, /* refCon */
	{ 0, 0, 0, 0 }, /* zoom rect */
	0x00000000, /* color table */
	{ 0, 0 }, /* origin */
	{ 0, 0 }, /* data height/width */
	{ 0, 0 }, /* max height/width */
	{ 0, 0 }, /* scroll vert/horz */
	{ 0, 0 }, /* page vert/horz */
	0x00000000, /* info refCon */
	0, /* info height */
	{ 50, 120, 150, 520 }, /* position */
	infront, /* plane */
	0x00000001, /* controlList */
	0x0009, /* verb */
};

resource rControlList(0x00000001) {
	{
		0x00000001,
		0x00000002,
		0x00000003
	}
};


#include "gsos_frotz.h"

#include <misctool.h>

#ifdef __ORCAC__
segment "gsos";
#endif

void os_beep(int number) {
	SysBeep();
}


void os_prepare_sample(int number) {
}

void os_finish_with_sample(int number) {
}

void os_start_sample(int number, int volume, int repeats, zword eos) {
}

void os_stop_sample(int number) {
}

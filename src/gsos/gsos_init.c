#ifdef __ORCAC__
segment "gsos";
#pragma debug 0x8000
#endif

#include "gsos_frotz.h"
#include "console.h"

#include <setjmp.h>
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <misctool.h>

#define GSString(size) struct { Word length; char text[size];}

#define MakeGSString(name, s) GSString(sizeof(s)) name = { sizeof(s) -1, s }



DIORecGS ioDCB = { 6 };
DAccessRecGS deviceDCB = { 5 };

static RefNumRecGS closeDCB = { 1 };

struct gsos_setup gs_setup;

void os_init_setup(void) {
	gs_setup.current_style = NORMAL_STYLE;
	gs_setup.screen_width = 80;
	gs_setup.upper = 0;
	gs_setup.console_active = 0;
}


// static TextPortRec SavedTextPort;
static unsigned char SavedTerminators[516];
static word SavedTerminatorsSize;

void gsos_setup_terms(void) {

	static char terms[] = {
		0x7f, 0x20 | 0x80, // term mask
		14,   0x00, // 14 terminators
		0x0d, 0x00, // return.
		0x0a, 0x20, // down-arrow - interrupt
		0x0b, 0x20, // up-arrow - interrupt
		0x09, 0x20, // tab - interrupt
		'o',  0x20 | 0x80,
		'O',  0x20 | 0x80,
		'q',  0x20 | 0x80,
		'Q',  0x20 | 0x80,
		'r',  0x20 | 0x80,
		'R',  0x20 | 0x80,
		's',  0x20 | 0x80,
		'S',  0x20 | 0x80,
		'z',  0x20 | 0x80,
		'Z',  0x20 | 0x80,
	};


	deviceDCB.code = SetTerminators;
	deviceDCB.list = terms;
	deviceDCB.requestCount = sizeof(terms);
	DControlGS(&deviceDCB);
}

void os_init_screen(void) {


	static char init80[] = {
		0x01, // save text port
		0x12, // 80-column mode
		0x0e, // normal display mode
		0x18, // disable mouse text
		0x02, 0 + 32, 0 + 32, 79 + 32, 23 + 32, // set text port
		0x0c, // home / clear
		// consDLE = off, consScroll = off, consWrap = off, consLF = off, consAdvance = on
		0x15, 0x01,
	};

	static char init40[] = {
		0x01, // save text port
		0x11, // 40-column mode
		0x0e, // normal display mode
		0x18, // disable mouse text
		0x02, 0 + 32, 0 + 32, 39 + 32, 23 + 32, // set text port
		0x0c, // home / clear
		// consDLE = on, consScroll = off, consWrap = off, consLF = off, consAdvance = on
		0x15, 0x11,
	};



	static MakeGSString(console_path, ".console");

	static DevNumRecGS dnDCB = { 2, 
	(GSString32Ptr)&console_path, 0};

	static OpenRecGS openDCB = { 3, 0, 
	(GSString255Ptr)&console_path, readWriteEnable };

	GetDevNumberGS(&dnDCB);

	ioDCB.devNum = dnDCB.devNum;
	deviceDCB.devNum = dnDCB.devNum;

	OpenGS(&openDCB);
	closeDCB.refNum = openDCB.refNum;


	/* save the old terminators */
	deviceDCB.code = GetTerminators;
	deviceDCB.list = SavedTerminators;
	deviceDCB.requestCount = sizeof(SavedTerminators);
	DStatusGS(&deviceDCB);
	SavedTerminatorsSize = _toolErr ? 0 : deviceDCB.transferCount;


	/* initialize the text input */

	gsos_setup_terms();

	if (gs_setup.screen_width == 40) {
		ioDCB.buffer = init40;
		ioDCB.requestCount = sizeof(init40);
	} else {
		ioDCB.buffer = init80;
		ioDCB.requestCount = sizeof(init80);
	}
	DWrite(&ioDCB);

	gs_setup.console_active = 1;

	z_header.screen_width = gs_setup.screen_width;
	z_header.screen_height = 24;
	z_header.font_height = 1;
	z_header.font_width = 1;

	z_header.screen_rows = 24;
	z_header.screen_cols = gs_setup.screen_width;

	z_header.interpreter_version = 'F';
	z_header.interpreter_number = INTERP_APPLE_IIGS;

	z_header.default_foreground = WHITE_COLOUR;
	z_header.default_background = BLACK_COLOUR;
	z_header.flags &= ~COLOUR_FLAG;

	if (z_header.version == V3)
		z_header.config |= CONFIG_SPLITSCREEN;

	if (z_header.version == V3)
		z_header.flags &= ~OLD_SOUND_FLAG;

	if (z_header.version >= V4)
		z_header.config |= CONFIG_FIXED | CONFIG_TIMEDINPUT;

	if (z_header.version >= V5)
		z_header.flags &= ~(GRAPHICS_FLAG | MENU_FLAG | MOUSE_FLAG | SOUND_FLAG);

	if (z_header.version >= V5) {
		if (f_setup.undo_slots == 0)
			z_header.flags &= ~UNDO_FLAG;
	}

	if (z_header.version == V6) {
		z_header.flags &= ~GRAPHICS_FLAG;
	}

}

void os_init_sound(void) {	
}

int	os_random_seed(void) {
	// TODO -- option to set the random seed
	return GetTick() & 0x7fff;
}

static void help(int ex) {

	fputs(
		"frotz [-h] [-4] [-U] zfile\n"
		" -h   Display help\n"
		" -4   40-column mode\n"
		" -U   UPPERCASE\n"
		,
		stderr
	);
	exit(ex);
}

void os_process_arguments (int argc, char *argv[]) {

	int c;

	f_setup.undo_slots = 5;

	if (gs_setup.desktop) {
		f_setup.story_file = gs_setup.story_file;
		f_setup.story_name = gs_setup.story_name;

		// set up default names for saving, etc?


		return;
	}

	while (( c = zgetopt(argc, argv, "h4U")) != -1) {
		switch (c) {
		case '4':
			gs_setup.screen_width = 40;
			break;
		case 'U':
			gs_setup.upper = 1;
			break;
		case 'h':
			help(0);
			break;
		case '?':
		case ':':
		default:
			help(1);
			break;
		}
	}

	argc -= zoptind;
	argv += zoptind;

	if (argc != 1) help(1);

	f_setup.story_file = argv[0];
	f_setup.story_name = argv[0];
}


FILE *os_path_open(const char *name, const char *mode) {
	FILE *fp;

	if ((fp = fopen(name, mode)))
		return fp;

	return NULL;
}

FILE *os_load_story(void) {
	// TODO -- blorb support?
	// return os_path_open("@:hitchhiker", "rb");
	// TODO -- environment variable for location?
	return os_path_open(f_setup.story_file, "rb");
}

int os_storyfile_seek(FILE * fp, long offset, int whence) {
	// TODO -- blorb support?

	return fseek(fp, offset, whence);
}

int os_storyfile_tell(FILE * fp) {
	// TODO -- blorb support?
	return ftell(fp);
}


void os_restart_game (int stage) {
}


extern jmp_buf gsos_jmp_buf;

void os_quit(int status) {

	// orca shell uses the .console driver so restore it.

	static char cmd[] = {
		0x12, // 80-column
		0x04, // pop text port
		0x0d,
	};

	ioDCB.buffer = cmd;
	ioDCB.requestCount = sizeof(cmd);
	DWrite(&ioDCB);


	// restore previous terminators.
	deviceDCB.code = SetTerminators;
	deviceDCB.list = SavedTerminators;
	deviceDCB.requestCount = SavedTerminatorsSize;
	DControlGS(&deviceDCB);


	CloseGS(&closeDCB);

	if (gs_setup.desktop) {
		longjmp(gsos_jmp_buf, 1);
		return;
	}

	exit(status);
}


static void alert_message(const char *text) {

	// here's the thing... os_fatal() might be called
	// before the .console driver is set up
	// eg, there's an error reading the infocom .z file

	if (gs_setup.console_active) {

		uint16_t mode;
		char c;

		iigs_start_box();

		ioDCB.buffer = text;
		ioDCB.requestCount = strlen(text);
		DWrite(&ioDCB);

		static char ok[] = {
			scDisableMouse,
			// 1
			scGotoXY, 32 + 00, 32 + 4, scSetInverse, ' ', 'O', 'k', ' ', scSetNormal,
		};

		if (gs_setup.screen_width == 40) {
			ok[2] = 32 + 36 - 6 - 4;
		} else {
			ok[2] = 32 + 60 - 6 - 4;
		}

		ioDCB.buffer = ok;
		ioDCB.requestCount = sizeof(ok);
		DWrite(&ioDCB);

		deviceDCB.code = SetReadMode;
		deviceDCB.list = (Pointer)&mode;
		deviceDCB.requestCount = 2;
		deviceDCB.transferCount = 0; 

		mode = 0x8000; // raw
		DControl(&deviceDCB);

		ioDCB.buffer = &c;
		ioDCB.requestCount = 1;
		for(;;) {
			DRead(&ioDCB);
			if (c == 0x0d) break;
		}

		mode = 0x0000;
		DControl(&deviceDCB);

		iigs_end_box();
	} else {

		// console not open ... 

		static struct {
			unsigned offset1;
			unsigned offset2;
			char text[30];
		} template = { 4, 2, "13/*0/^Ok/\x00" "000" };

		AlertMessage((Ptr)&template, 0, (Ptr)&text);
	}
}

// not called by common
void os_warn (const char *s, ...) {
	va_list m;
	static char errorstring[82];
	int n;

	va_start(m, s);
	n = vsnprintf(errorstring + 1, 80, s, m);
	va_end(m);
	errorstring[0] = n;

	SysBeep();

	alert_message(errorstring);

	#if 0
	if (gs_setup.desktop) {
		// TODO -- alert window...

		return;
	}

	// check textport.consVideo for inverse status?

	ioDCB.buffer = "\x0d\x0f" "Warning:" "\x0e" " ";
	ioDCB.requestCount = 12;
	DWrite(&ioDCB);

	ioDCB.buffer = errorstring + 1;
	ioDCB.requestCount = n;
	DWrite(&ioDCB);

	ioDCB.buffer = "\x0d\x0a";
	ioDCB.requestCount = 2;
	DWrite(&ioDCB);
	#endif
} /* os_warn */


// not actually varadic.
void os_fatal (const char *s, ...) {
	va_list m;
	static char errorstring[82];
	int n;

	va_start(m, s);
	n = vsnprintf(errorstring + 1, 80, s, m);
	va_end(m);
	errorstring[0] = n;

	SysBeep();

	alert_message(errorstring);

	if (gs_setup.desktop) {
		// TODO -- alert window...

		longjmp(gsos_jmp_buf, 2);
		return;
	}
	os_quit(EXIT_FAILURE);

#if 0
	// check textport.consVideo for inverse status?

	ioDCB.buffer = "\x0d\x0f" "Fatal:" "\x0e" " ";
	ioDCB.requestCount = 1;
	DWrite(&ioDCB);

	ioDCB.buffer = errorstring + 1;
	ioDCB.requestCount = n;
	DWrite(&ioDCB);

	ioDCB.buffer = "\x0d\x0a";
	ioDCB.requestCount = 2;
	DWrite(&ioDCB);

	os_read_key(0, 0);
	os_quit(EXIT_FAILURE);
#endif
} /* os_warn */


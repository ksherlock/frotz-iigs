#ifdef __ORCAC__
segment "gsos";
#endif

#include "gsos_frotz.h"

void os_erase_area(int top, int left, int bottom, int right, int win) {

	--top;
	//--left;
	--bottom;
	//--right;

	if (top == 0 && bottom == 23) {
		static char commands[] = {
			0x0c
		};

		ioDCB.buffer = commands;
		ioDCB.requestCount = sizeof(commands);

	} else {
		// set the text port, and clear it
		static char commands[] = {
			0x02, 0 + ' ', -1, 79 + ' ', -1,
			0x0c,
			0x02, 0 + ' ', 0 + ' ', 79 + ' ', 23 + ' '
		};

		commands[2] =  top + ' ';
		commands[4] = bottom + ' ';

		ioDCB.buffer = commands;
		ioDCB.requestCount = sizeof(commands);
	}
	DWrite(&ioDCB);

}

void os_scroll_area(int top, int left, int bottom, int right, int units) {

	static char scroll_down[] = {
		// 0x16 - scroll down 1 line. does not move the cursor.
		0x16, 0x16, 0x16, 0x16, 0x16, 0x16, 0x16, 0x16,
		0x16, 0x16, 0x16, 0x16, 0x16, 0x16, 0x16, 0x16,
		0x16, 0x16, 0x16, 0x16, 0x16, 0x16, 0x16, 0x16,
	};
	static char scroll_up[] = {
		// 0x17 - scroll up 1 line. does not move the cursor.
		0x17, 0x17, 0x17, 0x17, 0x17, 0x17, 0x17, 0x17,
		0x17, 0x17, 0x17, 0x17, 0x17, 0x17, 0x17, 0x17,
		0x17, 0x17, 0x17, 0x17, 0x17, 0x17, 0x17, 0x17,
	};

	int simple;

	--top;
	--bottom;


	simple = (top == 0 && bottom == 23);

	if (!simple) {
		// set text port.

		static char commands[] = {
			0x02, 0 + ' ', -1, 79 + ' ', -1,
		};

		commands[2] =  top + ' ';
		commands[4] = bottom + ' ';

		ioDCB.buffer = commands;
		ioDCB.requestCount = sizeof(commands);
		DWrite(&ioDCB);
	}

	ioDCB.buffer = units > 0 ? scroll_up : scroll_down;
	ioDCB.requestCount = units > 0 ? units : -units;
	DWrite(&ioDCB);

	if (!simple) {
		// reset text port.

		static char commands[] = {
			0x02, 0 + ' ', 0 + ' ', 79 + ' ', 23 + ' '
		};

		ioDCB.buffer = commands;
		ioDCB.requestCount = sizeof(commands);
		DWrite(&ioDCB);
	}
}


bool os_repaint_window(int win, int ypos_old, int ypos_new, int xpos,
		       int ysize, int xsize)
{
	return FALSE;
}

void os_reset_screen (void) {

	if (gs_setup.desktop) return;

	os_set_text_style(0);
	ioDCB.buffer = "\x0d\x0a" "[Hit any key to exit]";
	ioDCB.requestCount = 23;
	DWrite(&ioDCB);
	os_read_key(0, 0);

	ioDCB.buffer = "\x0c"; // clear and home
	ioDCB.requestCount = 1;
	DWrite(&ioDCB);
}

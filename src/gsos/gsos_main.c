
#ifdef __ORCAC__
segment "gsos";
#pragma debug 0x8000
#endif

#include <control.h>
#include <desk.h>
#include <event.h>
#include <gsos.h>
#include <locator.h>
#include <memory.h>
#include <menu.h>
#include <quickdraw.h>
#include <resources.h>
#include <stdfile.h>
#include <window.h>

#include <stdlib.h>
#include <string.h>
#include <setjmp.h>


#include "gsos_rez.h"
#include "gsos_frotz.h"


jmp_buf gsos_jmp_buf;

extern int frotz_main(int argc, char **argv);

static char *gs2cstr(GSString255Ptr str) {
	char *cp;
	unsigned l = str->length;
	cp = malloc(l + 1);
	if (cp) {
		memcpy(cp, str->text, l);
		cp[l] = 0;
	}
	return cp;
}

char *gsos_read_file_name(const char *default_name, int flag) {


	static SFReplyRec2 rr = {
		0, 0, 0, 3, 0, 3, 0
	};

	Handle h;
	char *cp;
	ResultBuf255 *rb;
	unsigned n;


	if (flag == FILE_SAVE || flag == FILE_SCRIPT) {
		static GSString255 name;
		static NameRecGS destroyDCB = { 1, 0 };
		static CreateRecGS createDCB = { 4, 0, 0xe3, 0, 0 };

		// default name (f_setup.save_name) not set up.
		#if 0
		n = strlen(default_name);
		name.length = n;
		memcpy(name.text, default_name, n);
		#endif

		n = strlen(gs_setup.story_name);
		memcpy(name.text, gs_setup.story_name, n);
		name.length = n;

		switch(flag) {
		case FILE_SAVE:
			memcpy(name.text + n, EXT_SAVE, sizeof(EXT_SAVE));
			name.length = n + sizeof(EXT_SAVE) - 1;
			break;

		case FILE_SCRIPT:
			memcpy(name.text + n, EXT_SCRIPT, sizeof(EXT_SCRIPT));
			name.length = n + sizeof(EXT_SCRIPT) - 1;
			break;
		}

		rr.good = 0;
		GrafOn();
		SFPutFile2(120, 40, 0, (Ref)"\pSave File\xc9", 0, (Ref)&name, &rr);
		GrafOff();
		// if (_toolErr) return NULL;
		if (!rr.good) return NULL;


		h = (Handle)rr.nameRef;
		DisposeHandle(h);

		h = (Handle)rr.pathRef;
		HLock(h);
		rb = *(ResultBuf255 **)h;


		// Create file w proper file/aux type

		destroyDCB.pathname = &rb->bufString; 
		createDCB.pathname = &rb->bufString;
		if (flag == FILE_SAVE) {
			createDCB.fileType = 0x5d; // game/entertainment document
			createDCB.auxType = 'Q';
		} else {
			createDCB.fileType = 0x04; // txt
			createDCB.auxType = 0;
		}

		DestroyGS(&destroyDCB);
		CreateGS(&createDCB);

		cp = gs2cstr(&rb->bufString);
		DisposeHandle(h);
		return cp;
	}

	if (flag == FILE_RESTORE) {
		static SFTypeList2 list = {
			3,
			{
				{ 0x0000, 0x00,0x0000 }, // non
				{ 0x8000, 0x06,0x0000 }, // bin
				{ 0x8000, 0xf5,0x0000 }, // user #5
			}
		};

		rr.good = 0;
		GrafOn();
		SFGetFile2(120, 40, 0, (Ref)"\pOpen File\xc9", NULL, &list, &rr);
		GrafOff();
		// if (_toolErr) return NULL;
		if (!rr.good) return NULL;

		h = (Handle)rr.nameRef;
		DisposeHandle(h);

		h = (Handle)rr.pathRef;
		HLock(h);
		rb = *(ResultBuf255 **)h;
		cp = gs2cstr(&rb->bufString);
		DisposeHandle(h);
		return cp;
	}

	return NULL;
}


static void DoOpen(void) {

	static SFTypeList2 list = {
		3,
		{
			{ 0x0000, 0x00,0x0000 }, // non
			{ 0x8000, 0x06,0x0000 }, // bin
			{ 0x8000, 0xf5,0x0000 }, // user #5
		}
	};

	static SFReplyRec2 rr = {
		0, 0, 0, 3, 0, 3, 0
	};

	Handle h;

	CloseAllNDAs(); /* if an NDA is open, GetNextEvent will pass key events to it */

	// window is 400 x 114 
	SFGetFile2(120, 40, 0, (Ref)"\pOpen File\xc9", NULL, &list, &rr);
	if (_toolErr) return;

	if (!rr.good) return;

	h = (Handle)rr.nameRef;
	if (h) {
		HLock(h);
		ResultBuf255 *rb = *(ResultBuf255 **)h;

		// TODO -- strip an extension?
		gs_setup.story_name = gs2cstr(&rb->bufString);
		DisposeHandle(h);
		rr.nameRef = NULL;
	}

	h = (Handle)rr.pathRef;
	if (h) {
		HLock(h);
		ResultBuf255 *rb = *(ResultBuf255 **)h;

		gs_setup.story_file = gs2cstr(&rb->bufString);
		DisposeHandle(h);
		rr.pathRef = NULL;
	}

	GrafOff();

	if (setjmp(gsos_jmp_buf) == 0) {

		frotz_main(0, NULL);

	} else {

		// longjmp!
	}

	GrafOn();

	// todo -- save so we can restart?
	free(gs_setup.story_file);
	free(gs_setup.story_name);
	gs_setup.story_name = NULL;
	gs_setup.story_file = NULL;
}

static unsigned EndDialog;

#pragma toolparms 1
#pragma databank 1

pascal void WindowDrawControls(void) {
	DrawControls(GetPort());
}
pascal void NullWindowDraw(void)
{ }

static unsigned EndDialog = 0;
static void SimpleBeepProc(WmTaskRec *event) {
	EndDialog = 1;
}

void SimpleEventHook(WmTaskRec *event) {
	if (!event) return;

	unsigned what = event->what;
	if (what == keyDownEvt || what == autoKeyEvt || what == mouseDownEvt) {
		EndDialog = 1;
	}
}

#pragma toolparms 0
#pragma databank 0

#if 0
static void DoAbout(void) {

	static WmTaskRec event;

	GrafPortPtr win;
	long id;

	win = NewWindow2(NULL, NULL, WindowDrawControls, NULL, refIsResource, kAboutWindow, rWindParam1);
	DrawControls(win);

	event.wmTaskMask = 0x001FFFFF;
	EndDialog = 0;
	unsigned state = 0;
	for(;;) {
		#define flags mwUpdateAll | mwDeskAcc
		id = DoModalWindow(&event, NullWindowDraw, (VoidProcPtr)SimpleEventHook, (VoidProcPtr)SimpleBeepProc, flags);
		#undef flags

		if (id) EndDialog = 1;

		if (EndDialog) {
			++state;
			if (state > 1)
				break;
			EndDialog = 0;

			// ShowHide(0, win); // this is here to prevent about text 3 redrawing multiple times.
			HideControl(GetCtlHandleFromID(win, kAboutText1));
			HideControl(GetCtlHandleFromID(win, kAboutText2));
			ShowControl(GetCtlHandleFromID(win, kAboutText3));
			// ShowHide(1, win);
		}
	}
	CloseWindow(win);

}
#else

static void DoAbout(void) {

	static WmTaskRec event;

	GrafPortPtr win;

	win = NewWindow2(NULL, NULL, NULL, NULL, refIsResource, kAboutWindow, rWindParam1);

	/*
	  so....
	  when we hide and show the controls, they redraw. but they also invalidate the window region.
	  having an invalid window region generates the updateEvent
	  (which is only cleared by begin update / end update)
	  double drawing is visibly noticable so we do extra work to prevent it from happening.
	*/

	event.wmTaskMask = 0;
	unsigned page = 0;
	unsigned redraw = 1;
	while (page < 2) {

		word taskCode = TaskMaster(0xffff, &event);
		switch(taskCode) {

		case updateEvt:
			BeginUpdate(win);
			if (redraw) DrawControls(win);
			EndUpdate(win);
			redraw = 1;
			break;

		case autoKeyEvt:
		case keyDownEvt:
		case mouseDownEvt:
			++page;
			if (page == 1) {
				HideControl(GetCtlHandleFromID(win, kAboutText1));
				HideControl(GetCtlHandleFromID(win, kAboutText2));
				ShowControl(GetCtlHandleFromID(win, kAboutText3));
				redraw = 0;
			}
			break;

		}
	}
	CloseWindow(win);
}

#endif

int main(int argc, char **argv) {

	static WmTaskRec event;
	static QuitRecGS quitDCB = { 2, 0, 0 };

	unsigned MyID;
	Ref tlRef;
	unsigned quit = 0;

	if (argc) {
		// run as an exe
		gs_setup.desktop = 0;
		return frotz_main(argc, argv);
	}
	gs_setup.desktop = 1;

	TLStartUp();
	MyID = MMStartUp();

	tlRef = StartUpTools(MyID, refIsResource, (Ref)kStartStop);


	SetSysBar(NewMenuBar2(2, kMenuBar, 0));
	SetMenuBar(0);
	FixAppleMenu(kAppleMenu);

	FixMenuBar();
	DrawMenuBar();
	InitCursor();

	event.wmTaskMask = 0x001FFFFF;

	while(!quit) {

		word taskCode = TaskMaster(0xffff, &event);
		unsigned item;
		unsigned menu;

		switch (taskCode) {
		case wInSpecial:
		case wInMenuBar:

			item = event.wmTaskData & 0xffff;
			menu = event.wmTaskData >> 16;

			switch(item) {
				case kQuitMenuItem:
					quit = 1;
					break;
				case kOpenMenuItem:
					DoOpen();
					break;
				case kAboutMenuItem:
					DoAbout();
					break;
			}
			HiliteMenu(0, menu);
			break;
		}
	}

	ShutDownTools(refIsHandle, tlRef);
	MMShutDown(MyID);
	TLShutDown();
	QuitGS(&quitDCB);
}
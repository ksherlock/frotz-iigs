
#ifdef __ORCAC__
segment "gsos";
#pragma debug 0x8000
#endif

#include "gsos_frotz.h"

#include <ctype.h>

int current_style = NORMAL_STYLE;

int os_font_data (int font, int *height, int *width) {
	if (font == TEXT_FONT) {
		*height = 1;
		*width = 1;
		return 1; /* Truth in advertising */
	}
	return 0;
}

void os_set_colour (int new_foreground, int new_background) {
}

void os_set_font(int new_font) {
}

int os_get_text_style (void) {
	return current_style;
}

void os_set_text_style(int new_style) {
	unsigned delta = new_style ^ current_style;
	current_style = new_style;

	if (delta & REVERSE_STYLE) {
		// flip-flop the reverse.
		// 0x0e = normal display mode
		// 0x0f = inverse display mode
		ioDCB.requestCount = 1;
		ioDCB.buffer = "\x0e\x0f" + (new_style & REVERSE_STYLE);
		DWrite(&ioDCB);
	}
}



void os_display_char(zchar c) {
	static char space[] = "   ";

	if (c >= ZC_ASCII_MIN && c <= ZC_ASCII_MAX) {
		if (gs_setup.upper) c = toupper(c);
		ioDCB.buffer = &c;
		ioDCB.requestCount = 1;
	} else if (c == ZC_GAP) {
		ioDCB.buffer = space;
		ioDCB.requestCount = 2;
	} else if (c == ZC_INDENT) {
		ioDCB.buffer = space;
		ioDCB.requestCount = 3;
	} else {
		return;
	}
	DWrite(&ioDCB);
}

void os_display_string(const zchar * s) {

	static char buffer[80];

	zchar c;
	unsigned i = 0;
	unsigned j = 0;

	ioDCB.buffer = buffer;

	while ((c = s[i++])) {

		if (j >= 70) {
			ioDCB.requestCount = j;
			DWrite(&ioDCB);
			j = 0;
		}

		if (c >= ZC_ASCII_MIN && c <=  ZC_ASCII_MAX) {
			if (gs_setup.upper) c = toupper(c);
			buffer[j++] = c;
		} else if (c == ZC_GAP) {
			buffer[j++] = ' ';
			buffer[j++] = ' ';
		} else if (c == ZC_INDENT) {
			buffer[j++] = ' ';
			buffer[j++] = ' ';
			buffer[j++] = ' ';			
		} else if (c == ZC_NEW_STYLE) {
			unsigned arg = s[i++];

			// 0x0e = normal display mode
			// 0x0f = inverse display mode

			unsigned delta = arg ^ current_style;
			current_style = arg;
			if (delta & REVERSE_STYLE) {
				if (arg & REVERSE_STYLE) buffer[j++] = 0x0f;
				else buffer[j++] = 0x0e;
			}

		} else if (c == ZC_NEW_FONT) {
			++i;
		}
	}

	if (j) {
		ioDCB.requestCount = j;
		DWrite(&ioDCB);
	}
}

int os_check_unicode(int font, zchar c) {
	return 0;
}

int os_char_width(zchar c) {

	if (c >= ZC_ASCII_MIN && c <= ZC_ASCII_MAX)
		return 1;

	if (c == ZC_GAP) return 2;
	if (c == ZC_INDENT) return 3;

	return 0;
}

int os_string_width(const zchar *s) {
	zchar c;
	unsigned i = 0;
	unsigned rv = 0;

	while ((c = s[i++])) {

		if (c >= ZC_ASCII_MIN && c <= ZC_ASCII_MAX)
			rv += 1;
		else if (c == ZC_GAP)
			rv += 2;
		else if (c == ZC_INDENT)
			rv += 3;
		else if (c == ZC_NEW_STYLE || c == ZC_NEW_FONT) {
			++i;
		}
	}
	return rv;
}


void os_set_cursor(int y, int x) {
	static char commands[] = { 0x1e, -1, -1};
	--x;
	--y;
	commands[1] = x + ' ';
	commands[2] = y + ' ';

	ioDCB.buffer = commands;
	ioDCB.requestCount = sizeof(commands);
	DWrite(&ioDCB);
}

void os_more_prompt(void) {
	// 0x0e = normal display mode
	// 0x0f = inverse display mode
	// 0x1a = clear current line, set x = 0
	static char more[] = "\x0e" "[More]";

	// we need to save/restore the x-position.

	ioDCB.buffer = more;
	ioDCB.requestCount = sizeof(more) - 1;
	DWrite(&ioDCB);

	os_read_key(0, 0);

	if (current_style & REVERSE_STYLE) {
		ioDCB.buffer = "\x1a" "\x0f";
		ioDCB.requestCount = 2;
	} else {
		ioDCB.buffer = "\x1a";
		ioDCB.requestCount = 1;
	}
	DWrite(&ioDCB);

	// set horizontal pos = 0, clear line
	ioDCB.buffer = "\x14 \x03";
	ioDCB.requestCount = 3;
	DWrite(&ioDCB);
}

int os_from_true_colour(zword colour) {
	return 0;
}

zword os_to_true_colour(int index) {
	return 0;
}


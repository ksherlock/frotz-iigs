#include <gsos.h>

#include "../common/frotz.h"

/* I don't like this one (.scr) */
#undef EXT_SCRIPT
#define EXT_SCRIPT ".log"

extern DIORecGS ioDCB;
extern DAccessRecGS deviceDCB;

struct gsos_setup {
	int current_style;
	int upper;
	int screen_width;
	int desktop;
	char *story_file;
	char *story_name;
	int console_active;
};

extern struct gsos_setup gs_setup;

void gsos_setup_terms(void);
void iigs_start_box(void);
void iigs_end_box(void);

